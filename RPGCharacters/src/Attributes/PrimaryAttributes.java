package Attributes;
import Player.Equipment;

public class PrimaryAttributes {
    private int vitality;
    private int strength;
    private int dexterity;
    private int intelligence;

    /**
     * Constructor for the primary attributes of the character.
     * This is used to set the base attributes according to each class's starting attributes.
     * @param vitality Characters starting vitality.
     * @param strength Characters starting strength.
     * @param dexterity Characters starting dexterity
     * @param intelligence Characters starting intelligence.
     */
    public PrimaryAttributes(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * This constructor is used to create the totalAttributes for the character.
     * @param attributes The base primaryAttributes of the character.
     */
    public PrimaryAttributes(PrimaryAttributes attributes) {
        this.vitality = attributes.getVitality();
        this.strength = attributes.getStrength();
        this.dexterity = attributes.getDexterity();
        this.intelligence = attributes.getIntelligence();
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }


    public int getVitality() {
        return vitality;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    /**
     * Used to update the primaryAttributes when a character equips new armor or gains a level.
     * @param primaryAttributes PrimaryAttributes of the character.
     * @param equipment The characters equipment.
     */
    public void update(PrimaryAttributes primaryAttributes, Equipment equipment) {
        this.vitality = (primaryAttributes.getVitality() + equipment.getVitality()) * 10;
        this.strength = (primaryAttributes.getDexterity() + primaryAttributes.getStrength() + equipment.getDexterity() + equipment.getStrength());
        this.dexterity = primaryAttributes.getIntelligence() + equipment.getIntelligence();
        this.intelligence = primaryAttributes.getIntelligence() + equipment.getIntelligence();
    }
    public void printAttributes() {
        System.out.println("Vitality: " + this.vitality);
        System.out.println("Strength: " + this.strength);
        System.out.println("Dexterity: " + this.dexterity);
        System.out.println("Intelligence: " + this.intelligence);

    }
}
