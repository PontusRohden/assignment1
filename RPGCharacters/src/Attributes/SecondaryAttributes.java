package Attributes;

import Player.Equipment;

public class SecondaryAttributes {
    private int health;
    private int armorRating;
    private int elementalResistance;

    /**
     * creates the secondaryAttributes for the character
     */
    public SecondaryAttributes() {
        this.health = 0;
        this.armorRating = 0;
        this.elementalResistance = 0;
    }

    public int getHealth() {
        return health;
    }


    public int getArmorRating() {
        return armorRating;
    }


    public int getElementalResistance() {
        return elementalResistance;
    }

    /**
     * Calculates and updates the secondaryAttributes for the character
     * @param primaryAttributes the characters totalAttributes.
     * @param equipment the characters equipment.
     */
    public void update(PrimaryAttributes primaryAttributes, Equipment equipment) {
        this.health = (primaryAttributes.getVitality() + equipment.getVitality()) *10;
        this.armorRating = (primaryAttributes.getDexterity() + primaryAttributes.getStrength() + equipment.getDexterity() + equipment.getStrength());
        this.elementalResistance = primaryAttributes.getIntelligence() + equipment.getIntelligence();
    }
}
