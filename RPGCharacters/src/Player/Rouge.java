package Player;

import Attributes.PrimaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.*;

public class Rouge extends RPGCharacter implements CharacterClass{
    private double dps = 0;

    /**
     * Creates a Rouge character with the class specific primaryAttributes.
     * @param name The name of the character.
     */
    public Rouge(String name) {
        super(name, new Equipment(),
        new PrimaryAttributes(8,2,6,1));
        updateDps();
    }

    /**
     * Used to equip the character with a new Item, Weapon or Armor.
     * @param item The item to be equipped.
     * @throws Exception InvalidWeaponException if the character are unable to use the weapon.
     * @throws Exception InvalidArmorException if the character are unable to use the armor.
     * Updates the characters dps after successful equip.
     */
    public void equip(Item item) throws Exception {
        if(item.getSlot() == SlotType.WEAPON) {
            if(((Weapon) item).getWeaponType()!= WeaponType.DAGGER && ((Weapon) item).getWeaponType() != WeaponType.STAFF){
                throw new InvalidWeaponException("Rouges can only equip Daggers or Swords!");
            }else{
                super.equipWeapon((Weapon) item);
            }
        }else{
            if(((Armor)item).getArmorType() != ArmorType.LEATHER && ((Armor)item).getArmorType() != ArmorType.LEATHER ) {
                throw new InvalidArmorException("Rouges can only use Leather or Mail Armor!");
            }else {
                super.equipArmor((Armor)item);
            }
        }
        updateDps();
    }

    /**
     * Calls the parent class levelUp method with the class-specific attributes.
     */
    public void levelUp() {
        super.levelUp(3,1,4,1);
    }

    /**
     * Updates the characters dps based on the classes dps increasing attribute.
     */
    @Override
    public void updateDps() {
        this.dps = ((1+(((double)super.getDexterity())/100)) + (super.getDamage()) * super.getAttacksPerSecond());
    }
    public double getDps(){
        return dps;
    }

    public void printCharacter() {
        super.printCharacter();
        System.out.println("Dps: " + this.dps);
    }
}