package Player;

import Attributes.SecondaryAttributes;
import Items.Armor;
import Items.Item;
import Items.SlotType;
import Items.Weapon;
import java.util.HashMap;
import java.util.Map;

public class Equipment {
    private HashMap<SlotType, Item> map;

    /**
     * Creates a HashMap for items to be stored in.
     */
    public Equipment() {
        map = new HashMap(4);
    }

    public void EquipArmor(Armor armor) {
        map.put(armor.getSlot(), armor);
    }

    public void EquipWeapon(Weapon weapon) {
        map.put(weapon.getSlot(), weapon);
    }

    /**
     * Searches the Map for everything that isn't a Weapon (i.e armor)
     * and adds the items vitality to the temp variable.
     * @return the total vitality of equipment
     */
    public int getVitality() {
        int temp = 0;
        for (Map.Entry<SlotType, Item> e : map.entrySet()) {
            if (e.getKey() != SlotType.WEAPON) {
                temp = ((Armor) e.getValue()).getAttributes().getVitality();
            }
        }
        return temp;
    }

    /**
     * Searches the Map for everything that isn't a Weapon (i.e armor)
     * and adds the items strength to the temp variable.
     * @return the total strength of equipment
     */
    public int getStrength() {
        int temp = 0;
        for (Map.Entry<SlotType, Item> e : map.entrySet()) {
            if (e.getKey() != SlotType.WEAPON) {
                temp = ((Armor) e.getValue()).getAttributes().getStrength();
            }
        }
        return temp;
    }

    /**
     * Searches the Map for everything that isn't a Weapon (i.e armor)
     * and adds the items dexterity to the temp variable.
     * @return the total dexterity of equipment
     */
    public int getDexterity(){
        int temp = 0;
        for (Map.Entry<SlotType, Item> e : map.entrySet()) {
            if (e.getKey() != SlotType.WEAPON) {
                temp = ((Armor) e.getValue()).getAttributes().getDexterity();
            }
        }
        return temp;
    }

    /**
     * Searches the Map for everything that isn't a Weapon (i.e armor)
     * and adds the items intelligence to the temp variable.
     * @return the total intelligence of equipment
     */
    public int getIntelligence() {
        int temp = 0;
        for (Map.Entry<SlotType, Item> e : map.entrySet()) {
            if (e.getKey() != SlotType.WEAPON) {
                temp = ((Armor) e.getValue()).getAttributes().getIntelligence();
            }
        }
        return temp;
    }

    /**
     * Searches the Map for an item with slotType WEAPON.
     * @return the damage of the equipped weapon, or 0;
     */
    public int getDamage() {
        for (Map.Entry<SlotType, Item> e : map.entrySet()) {
            if (e.getKey() == SlotType.WEAPON) {
                return ((Weapon) e.getValue()).getDamage();
            }
        }
        return 0;
    }

    /**
     * Searches the Map for an item with slotType WEAPON.
     * @return the attackSpeed of the equipped weapon, or 0;
     */
    public double attacksPerSecond() {
        for (Map.Entry<SlotType, Item> e : map.entrySet()) {
            if (e.getKey() == SlotType.WEAPON) {
                return ((Weapon) e.getValue()).getAttacksPerSecond();
            }
        }
        return 0;
    }

    public void printEquipment() {
        for (Map.Entry<SlotType, Item> e : map.entrySet()) {
            System.out.println((e.getKey() + ": " + e.getValue().getName()));
        }
    }
}
