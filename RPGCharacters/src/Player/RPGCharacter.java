package Player;

import Attributes.PrimaryAttributes;
import Attributes.SecondaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Armor;
import Items.Weapon;

public abstract class RPGCharacter {
    private String name;
    private Equipment equipment;
    private PrimaryAttributes primaryAttributes;
    private PrimaryAttributes totalAttributes;
    private SecondaryAttributes secondaryAttributes;
    private int level;

    /**
     * Called from the class-specific constructors, instantiates the rest of the character
     * @param name the characters name.
     * @param equipment the characters equipment.
     * @param primaryAttributes the characters primaryAttributes.
     */
    public RPGCharacter(String name, Equipment equipment, PrimaryAttributes primaryAttributes) {
        this.name = name;
        this.equipment = equipment;
        this.primaryAttributes = primaryAttributes;
        totalAttributes = new PrimaryAttributes(primaryAttributes);
        secondaryAttributes = new SecondaryAttributes();
        secondaryAttributes.update(this.primaryAttributes, this.equipment);
        level = 1;
    }

    /**
     * Increases the primary attributes when leveling up, based on class.
     * @param vitality
     * @param strength
     * @param dexterity
     * @param intelligence
     */
    public void levelUp(int vitality, int strength, int dexterity, int intelligence){
        primaryAttributes.setVitality(primaryAttributes.getVitality() + vitality);
        primaryAttributes.setStrength(primaryAttributes.getStrength() + strength);
        primaryAttributes.setDexterity(primaryAttributes.getDexterity() + dexterity);
        primaryAttributes.setIntelligence(primaryAttributes.getIntelligence() + intelligence);
        totalAttributes.update(this.primaryAttributes, this.equipment);
        secondaryAttributes.update(this.primaryAttributes, this.equipment);
        level++;

    }

    /**
     * Equips the character with armor.
     * @param armor armor to be equipped.
     * @return true if successfully equipped.
     * @throws Exception InvalidArmorException if the character level is to low.
     */
    public boolean equipArmor(Armor armor) throws Exception {
        if(this.level < armor.getLevelReq()) {
            throw new InvalidArmorException("Your level is to low to equip this armor");
        }else{
        equipment.EquipArmor(armor);
        totalAttributes.update(this.primaryAttributes, this.equipment);
        secondaryAttributes.update(this.totalAttributes, this.equipment);
        }
        return true;
    }
    /**
     * Equips the character with a weapon.
     * @param weapon weapon to be equipped.
     * @return true if successfully equipped.
     * @throws Exception InvalidWeaponException if the character level is to low.
     */
    public boolean equipWeapon(Weapon weapon) throws Exception {
        if (this.level < weapon.getLevelReq()) {
            throw new InvalidWeaponException("Your level is to low to equip this weapon");
        } else {
            equipment.EquipWeapon(weapon);
        }
        return true;
    }
    public void printCharacter() {
        System.out.println("Name: " + this.name);
        System.out.println("Level: " + this.level);
        System.out.println("--------");
        System.out.println("Equipment: ");
        equipment.printEquipment();
        System.out.println("--------");
        System.out.println("Primary attributes");
        totalAttributes.printAttributes();
        System.out.println("--------");
        System.out.println("Secondary attributes");
        System.out.println("Health: " + secondaryAttributes.getHealth());
        System.out.println("Armor rating: " + secondaryAttributes.getArmorRating());
        System.out.println("Elemental Resistance: " + secondaryAttributes.getElementalResistance());
    }
    public int getHealth() {return secondaryAttributes.getHealth();}
    public int getArmorRating() { return secondaryAttributes.getArmorRating();}
    public int getElementalResistance() { return secondaryAttributes.getElementalResistance();}
    public int getLevel(){ return this.level;}
    public int getVitality(){
        return equipment.getVitality() + primaryAttributes.getVitality();
    }
    public int getStrength() {
        return equipment.getStrength() + primaryAttributes.getStrength();
    }
    public int getDexterity() {
        return equipment.getDexterity()+ primaryAttributes.getDexterity();
    }
    public int getIntelligence() {
        return equipment.getIntelligence() + primaryAttributes.getIntelligence();
    }
    public int getDamage() {
        return equipment.getDamage();
    }
    public double getAttacksPerSecond() {
        return equipment.attacksPerSecond();
    }

}
