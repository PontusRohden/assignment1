package Player;

import Attributes.PrimaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.*;

public class Ranger extends RPGCharacter implements CharacterClass{
    private double dps = 0;

    /**
     * Creates a Ranger character with the class specific primaryAttributes.
     * @param name The name of the character.
     */
    public Ranger(String name) {

        super(name, new Equipment(),
        new PrimaryAttributes(8,1,7,1));
        updateDps();
    }

    /**
     * Used to equip the character with a new Item, Weapon or Armor.
     * @param item The item to be equipped.
     * @throws Exception InvalidWeaponException if the character are unable to use the weapon.
     * @throws Exception InvalidArmorException if the character are unable to use the armor.
     * Updates the characters dps after successful equip.
     */
    public void equip(Item item) throws Exception {
        if(item.getSlot() == SlotType.WEAPON) {
            if(((Weapon) item).getWeaponType()!= WeaponType.BOW){
                throw new InvalidWeaponException("Rangers can only equip Bows");
            }else{
                super.equipWeapon((Weapon) item);
            }
        }else{
            if(((Armor)item).getArmorType() != ArmorType.LEATHER && ((Armor)item).getArmorType() != ArmorType.MAIL) {
                throw new InvalidArmorException("Rangers can only use Leather or Mail armor!");
            }else {
                super.equipArmor((Armor)item);
            }
        }
        dps = getDps();
    }

    /**
     * Calls the parent class levelUp method with the class-specific attributes.
     */
    public void levelUp() {
        super.levelUp(2,1,5,1);
    }

    /**
     * Updates the characters dps based on the classes dps increasing attribute.
     */
    @Override
    public void updateDps() {
        this.dps = ((1+(((double)super.getDexterity())/100)) + (super.getDamage()) * super.getAttacksPerSecond());
    }

    public double getDps() {
        return this.dps;
    }

    public void printCharacter() {
        super.printCharacter();
        System.out.println("Dps: " + this.dps);
    }
}
