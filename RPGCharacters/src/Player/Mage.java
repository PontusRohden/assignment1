package Player;

import Attributes.PrimaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.*;


public class Mage extends RPGCharacter implements CharacterClass{
    private double dps = 0;

    /**
     * Creates a Mage character with the class specific primaryAttributes.
     * @param name The name of the character.
     */
    public Mage(String name) {
        super(name, new Equipment(),
        new PrimaryAttributes(5,1,1,8));
        updateDps();
    }

    /**
     * Used to equip the character with a new Item, Weapon or Armor.
     * @param item The item to be equipped.
     * @throws Exception InvalidWeaponException if the character are unable to use the weapon.
     * @throws Exception InvalidArmorException if the character are unable to use the armor.
     * Updates the characters dps after successful equip.
     */
    public void equip(Item item) throws Exception {
        if(item.getSlot() == SlotType.WEAPON) {
            if(((Weapon) item).getWeaponType()!= WeaponType.WAND && ((Weapon) item).getWeaponType() != WeaponType.STAFF){
                throw new InvalidWeaponException("Mages can only equip Wands or Staffs!");
            }else{
                super.equipWeapon((Weapon) item);
            }
        }else{
            if(((Armor)item).getArmorType() != ArmorType.CLOTH ) {
                throw new InvalidArmorException("Mages can only use Cloth Armor!");
            }else {
                super.equipArmor((Armor)item);
            }
        }
        updateDps();
    }

    /**
     * Calls the parent class levelUp method with the class-specific attributes.
     */
    public void levelUp() {
        super.levelUp(3,1,1,5);
    }

    /**
     * Updates the characters dps based on the classes dps increasing attribute.
     */
    @Override
    public void updateDps() {
        this.dps = ((1+(((double)super.getIntelligence())/100)) + (super.getDamage()) * super.getAttacksPerSecond());
    }

    public double getDps() {
        return this.dps;
    }

    public void printCharacter() {
        super.printCharacter();
        System.out.println("Dps: " + this.dps);
    }
}