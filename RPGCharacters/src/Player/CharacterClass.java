package Player;

import Items.Item;

/**
 * An interface to be used when creating new character classes.
 */
public interface CharacterClass {

    void equip(Item item) throws Exception;
    void levelUp();
    void updateDps();
    double getDps();
}
