package Player;

import Attributes.PrimaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.*;

public class Warrior extends RPGCharacter implements CharacterClass{
    private double dps = 0;

    /**
     * Creates a Warrior character with the class specific primaryAttributes.
     * @param name The name of the character.
     */
    public Warrior(String name) {
        super(name, new Equipment(),
        new PrimaryAttributes(10,5,2,1 ));
        updateDps();
    }

    /**
     * Used to equip the character with a new Item, Weapon or Armor.
     * @param item The item to be equipped.
     * @throws Exception InvalidWeaponException if the character are unable to use the weapon.
     * @throws Exception InvalidArmorException if the character are unable to use the armor.
     * Updates the characters dps after successful equip.
     */
    public void equip(Item item) throws Exception {
        if(item.getSlot() == SlotType.WEAPON) {
            if(((Weapon) item).getWeaponType()!= WeaponType.AXE && ((Weapon) item).getWeaponType() != WeaponType.HAMMER && ((Weapon) item).getWeaponType() != WeaponType.SWORD){
                throw new InvalidWeaponException("Warriors can only equip Axes, Hammers and Swords!");
            }else{
                super.equipWeapon((Weapon) item);
            }
        }else{
            if(((Armor)item).getArmorType() != ArmorType.MAIL && ((Armor)item).getArmorType() != ArmorType.PLATE ) {
                throw new InvalidArmorException("Warriors can only use Mail or Plate Armor!");
            }else {
                super.equipArmor((Armor)item);
            }
        }
        updateDps();
    }

    /**
     * Calls the parent class levelUp method with the class-specific attributes.
     */
    public void levelUp() {
        super.levelUp(5,3,2,1);
    }

    /**
     * Updates the characters dps based on the classes dps increasing attribute.
     */
    @Override
    public void updateDps() {
        this.dps = ((1+(((double)super.getDexterity())/100)) + ((super.getDamage()) * super.getAttacksPerSecond()));
    }
    public double getDps() {
        return dps;
    }

    public void printCharacter() {
        super.printCharacter();
        System.out.println("Dps: " + this.dps);
    }
}
