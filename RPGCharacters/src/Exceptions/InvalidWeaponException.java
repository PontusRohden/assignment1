package Exceptions;

public class InvalidWeaponException extends Exception{

    /**
     * Thrown when a character tries to use a weapon that it's not allowed to use.
     * @param message String to print for debugging.
     */
    public InvalidWeaponException(String message) {
        super(message);
    }
}
