package Exceptions;

public class InvalidArmorException extends Exception {

    /**
     * Thrown when a character tries to use an armor that it's not allowed to use.
     * @param message String to print for debugging.
     */
    public InvalidArmorException(String message) {
        super(message);
    }
}
