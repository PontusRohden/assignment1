import Attributes.PrimaryAttributes;
import Attributes.SecondaryAttributes;
import Items.*;
import Player.Mage;
import Player.Ranger;

public class Main {

    public static void main(String[] args) throws Exception {
        Weapon Staff = new Weapon("Magic Staff", 2,SlotType.WEAPON, WeaponType.STAFF, 3, 1.5);
        Weapon Wand = new Weapon("Magic Wand", 1,SlotType.WEAPON, WeaponType.WAND, 2, 2);
        Weapon Sword = new Weapon("Sword", 1,SlotType.WEAPON, WeaponType.SWORD, 5, 2);
        Weapon Dagger = new Weapon("Dagger", 1,SlotType.WEAPON, WeaponType.DAGGER, 3, 3);
        Weapon Axe = new Weapon("Axe", 1,SlotType.WEAPON, WeaponType.AXE, 8, 1.2);
        Weapon Bow = new Weapon("Bow", 1,SlotType.WEAPON, WeaponType.BOW, 6, 1.6);
        Weapon Hammer = new Weapon("Hammer", 1,SlotType.WEAPON, WeaponType.HAMMER, 14, 1);

        Armor clothArmor = new Armor("Armor of the Word", 1, SlotType.BODY, ArmorType.CLOTH, new PrimaryAttributes(1,2,1,2));
        Armor plateArmor = new Armor("Plate Armor", 1, SlotType.BODY,ArmorType.PLATE, new PrimaryAttributes(4,2,1,0) );

        Mage Lorgar = new Mage("Lorgar");
        Lorgar.levelUp();
        Lorgar.equipWeapon(Staff);
        Lorgar.equip(clothArmor);
        Lorgar.levelUp();
        Lorgar.printCharacter();

    }
}
