package Items;
import Attributes.PrimaryAttributes;

public class Armor extends Item {

    private ArmorType ArmorType;
    private PrimaryAttributes Attributes;

    /**
     * Creates a new armor
     * @param Name name of the armor piece.
     * @param LevelReq the required level to equip the armor.
     * @param Slot What slot type the armor will be equipped in.
     * @param ArmorType The armors material.
     * @param Attributes The primaryAttributes of the armor piece.
     */
    public Armor(String Name, int LevelReq, SlotType Slot, ArmorType ArmorType, PrimaryAttributes Attributes) {
        super(Name, LevelReq, Slot);
        this.ArmorType = ArmorType;
        this.Attributes = Attributes;
    }
    public Items.ArmorType getArmorType() {
        return ArmorType;
    }

    public PrimaryAttributes getAttributes() {
        return Attributes;
    }

    public void PrintItem() {
        System.out.println("Name: " + this.getName());
        System.out.println("Level Requirement: " + this.getLevelReq());
        System.out.println("Slot: " +this.getSlot());
        System.out.println("Armor type: " + this.ArmorType);
        System.out.println("Attributes: " + this.Attributes);
    }
}
