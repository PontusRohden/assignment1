package Items;

public enum SlotType {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
