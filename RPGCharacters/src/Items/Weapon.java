package Items;

public class Weapon extends Item {
    private WeaponType WeaponType;
    private int Damage;
    private double AttacksPerSecond;

    /**
     * Creates a new Weapon.
     * @param Name the name of the weapon.
     * @param LevelReq THe level required to equip the weapon.
     * @param Slot the items slotType (Always Weapon).
     * @param WeaponType What kind of weapon.
     * @param Damage The damage of the weapon.
     * @param AttacksPerSecond How often the character can attack with the weapon.
     */
    public Weapon(String Name, int LevelReq, SlotType Slot, WeaponType WeaponType, int Damage, double AttacksPerSecond) {
        super(Name, LevelReq, Slot);
        this.WeaponType = WeaponType;
        this.Damage = Damage;
        this.AttacksPerSecond = AttacksPerSecond;
    }
    public Items.WeaponType getWeaponType() {
        return WeaponType;
    }

    public int getDamage() {
        return Damage;
    }

    public double getAttacksPerSecond() {
        return AttacksPerSecond;
    }

    public void PrintItem() {
        System.out.println("Name: " + this.getName());
        System.out.println("Level Requirement: " + this.getLevelReq());
        System.out.println("Slot: " +this.getSlot());
        System.out.println("Weapon type: " +this.getWeaponType());
        System.out.println("Weapon damage: " +this.getDamage());
        System.out.println("Attacks per second: " +this.getAttacksPerSecond());
    }
}
