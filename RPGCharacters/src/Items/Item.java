package Items;

public abstract class Item {

    protected String Name;
    protected int LevelReq;
    protected SlotType Slot;

    /**
     * Creates an item, either weapon or armor, to equip to the character.
     * @param name The items name.
     * @param levelReq The required level to equip the item.
     * @param slot The slot were the item will be equipped.
     */
    public Item(String name, int levelReq, SlotType slot) {
        this.Name = name;
        this.LevelReq = levelReq;
        this.Slot = slot;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getLevelReq() {
        return LevelReq;
    }

    public void setLevelReq(int levelReq) {
        LevelReq = levelReq;
    }

    public SlotType getSlot() {
        return Slot;
    }

    public void setSlot(SlotType slot) {
        Slot = slot;
    }

    public abstract void PrintItem();
}
