package Items;

public enum ArmorType {
    CLOTH,
    MAIL,
    LEATHER,
    PLATE
}
