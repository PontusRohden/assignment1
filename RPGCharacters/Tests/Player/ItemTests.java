package Player;

import Attributes.PrimaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RPGCharacterTest {

    Mage Lorgar = new Mage("Lorgar");
    Armor plateArmor = new Armor("Plate Armor", 1, SlotType.BODY, ArmorType.PLATE, new PrimaryAttributes(4,2,1,0) );
    Armor clothArmor = new Armor("Cloth Armor", 1, SlotType.BODY, ArmorType.CLOTH, new PrimaryAttributes(1,2,1,2));
    Armor epicClothArmor = new Armor("Epic Cloth Armor", 2, SlotType.BODY, ArmorType.CLOTH, new PrimaryAttributes(1,2,1,2));
    Weapon staff = new Weapon("Magic Staff", 1,SlotType.WEAPON, WeaponType.STAFF, 3, 1.5);
    Weapon sword = new Weapon("Sword", 1,SlotType.WEAPON, WeaponType.SWORD, 5, 2);
    Weapon wand = new Weapon("Magic Wand", 2,SlotType.WEAPON, WeaponType.WAND, 3, 1.5);

    @Test
    void Mage_createNewCharacter_correctStarterAttributes() {
        assertEquals(5, Lorgar.getVitality());
        assertEquals(1, Lorgar.getStrength());
        assertEquals(1, Lorgar.getDexterity());
        assertEquals(8, Lorgar.getIntelligence());
    }
    @Test
    void levelUp() {
        Lorgar.levelUp();
        assertEquals(8, Lorgar.getVitality());
        assertEquals(2, Lorgar.getStrength());
        assertEquals(2, Lorgar.getDexterity());
        assertEquals(13, Lorgar.getIntelligence());
    }

    @Test
    void equipArmor_validArmor_shouldGiveCorrectAttributes() throws Exception {
        Lorgar.equip(clothArmor);
        assertEquals(6, Lorgar.getVitality());
        assertEquals(3, Lorgar.getStrength());
        assertEquals(2, Lorgar.getDexterity());
        assertEquals(10, Lorgar.getIntelligence());
    }
    @Test
    void equipArmor_invalidArmorType_shouldThrowException() {
        assertThrows(InvalidArmorException.class, () -> Lorgar.equip(plateArmor));
    }

    @Test
    void equipWeapon_validWeapon_shouldGiveCorrectDps() throws Exception {
        Lorgar.equip(staff);
        assertEquals(5.58, Lorgar.getDps());
    }
    @Test
    void equipWeapon_invalidWeaponType_shouldThrowException() {
        assertThrows(InvalidWeaponException.class, () -> Lorgar.equip(sword));
    }
    @Test
    void equipWeapon_characterLevelToLow_shouldThrowException(){
        assertThrows(InvalidWeaponException.class, () -> Lorgar.equip(wand));
    }
    @Test
    void equipArmor_characterLevelToLow_shouldThrowException() {
        assertThrows(InvalidArmorException.class, () -> Lorgar.equip(epicClothArmor));
    }
    @Test
    void equipWeapon_validWeapon_true() throws Exception {
        assertTrue(Lorgar.equipWeapon(staff));
    }
    @Test
    void equipArmor_validArmor_true() throws Exception {
        assertTrue(Lorgar.equipArmor(clothArmor));
    }
    @Test
    void getDps_noEquipment_equalsTrue() {
        assertEquals(1.08, Lorgar.getDps());
    }
    @Test
    void getDps_weaponAndArmor_equalsTrue() throws Exception {
        Lorgar.equip(staff);
        Lorgar.equip(clothArmor);
        assertEquals(5.6, Lorgar.getDps());
    }
}