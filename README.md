The first assignment for the Experis Academy program.  
The assignement:  
1) Optional: Class interaction diagrams  
    You can draw out the planning of the various classes and their interactions  
    to help visualize the application and its functionality.  
2) Use plain Java to create a console application with the following minimum requirements:  
    a) Various character classes having attributes which increase at different rates as the character gains levels.  
    b) Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the  
    power of the character, causing it to deal more damage and be able to survive longer.  
    certain characters can equip certain item types.  
    c) Custom exceptions. There are two custom exceptions you are required to write.  
    d) Full test coverage of the functionality. some testing data is provided, it can be usd to complete the assignment   
    in a test-driven development manner. 


This is the UML diagram i made as a preliminary sketch of the assignment:
![UML diagram](RPGCharacters/RPGChracter.png)


HowTo: 
  
1) Create a character.  
Declare one of the 4 different classes (Mage, Ranger, Rouge or Warrior) with a String name as parameter.  
    ex: Mage Merlin = new Mage("Merlin");  
        
2) equip item.  
use the equip method with an item as parameter.  
    ex: Merlin.equip(staff);  
There are some items created in the main file from the start.  
  
3) Level up character.  
Use the levelUp() method to add one level to your character.  
    ex: Merlin.levelUp()  
        
4) View character.  
Use the printCharacter method to see your characters name, level, equipment and stats.  
    ex: Melin.printCharacter();  


 
